const puppeteer = require('puppeteer')
const iPhone = puppeteer.devices['Iphone 6'];
const slow3G = puppeteer.networkConditions['Slow 3G'];


test('fill in form', async () => {
  const browser = await puppeteer.launch({ 
    headless: false,
    slowMo: 250
  });
  const page = await browser.newPage();
  await page.goto('file:///C:/Users/Nathan/testing/workshop/index.html')
  await page.type("input#name", "nathan")
  await page.type("input#age", "21")



})

test('take screenshot', async () => {
    const browser = await puppeteer.launch({ 
      headless: false,
      slowMo: 250
    });
    const page = await browser.newPage();
  await page.emulateNetworkConditions(slow3G);
  await page.emulate(iPhone);
  await page.setViewport({ width: 1920, height: 1080 });
  await page.goto('https://pptr.dev');
  await page.waitForSelector('title');

  await page.screenshot({
    path: 'screenshot.jpg',
    type: 'jpeg',
    quality: 80,
    clip: { x: 220, y: 0, width: 630, height: 360 }
  });
})

test.only('arrow movement', async () => {
  const browser = await puppeteer.launch({ 
    headless: false,
    slowMo: 250
});
const page = await browser.newPage();
await page.setViewport({ width: 1920, height: 1080 });
    await page.goto('https://pptr.dev');
    await page.waitForSelector('title');
    await page.focus('[type="search"]');
    await page.keyboard.type('Keyboard', { delay: 100 });
    await page.keyboard.press('ArrowDown', { delay: 200 });
    await page.keyboard.press('ArrowDown', { delay: 200 });
    await page.keyboard.press('Enter');
})

