const puppeteer = require('puppeteer')
const { evaluate } = require('aatt');


function cloneArray(array){
    return [...array]
}

test('should clone', () => {
    const data = [1,2,3]
    expect(cloneArray(data)).toEqual(data)
})


function min(a, b){
    return a - b
}

test('minus', () => {
    expect(min(2,1)).toBe(1)
})

function addFloat() {
    return 0.3 + 0.4
}

test('float', () => {
    expect(addFloat()).toBeCloseTo(0.7)
})


test('object assignment', () => {
    const data = {one: 1, two: 2}
    expect(data).toEqual({one: 1, two: 2})
  })
